#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

DEBDISTDIFF=${DEBDISTDIFF:-debdistdiff}

DISTS_DEBIAN=${DISTS_UBUNTU:-/tmp/debian}
DISTS_PUREOS=${DISTS_PUREOS:-/tmp/pureos}

PAIRS="bullseye:byzantium bookworm:crimson"

for pair in $PAIRS; do
    debian=$(echo $pair | cut -d: -f1)
    pureos=$(echo $pair | cut -d: -f2)

    for suffix in "" "-updates" "-security"; do
	time $DEBDISTDIFF -v --arch amd64 --dist $DISTS_DEBIAN --dist $DISTS_PUREOS \
	     $debian$suffix:main $pureos$suffix \
	     > diff-$debian-$pureos$suffix.txt
    done
done

exit 0
