#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

DEBDISTDIFF=${DEBDISTDIFF:-debdistdiff}

DISTS_UBUNTU=${DISTS_UBUNTU:-/tmp/ubuntu}
DISTS_TRISQUEL=${DISTS_TRISQUEL:-/tmp/trisquel}

PAIRS="bionic:etiona focal:nabia jammy:aramo"

for pair in $PAIRS; do
    ubuntu=$(echo $pair | cut -d: -f1)
    trisquel=$(echo $pair | cut -d: -f2)

    for suffix in "" "-updates" "-security" "-backports"; do
	time $DEBDISTDIFF -v --arch amd64 --dist $DISTS_UBUNTU --dist $DISTS_TRISQUEL \
	     $ubuntu$suffix:main,universe $trisquel$suffix \
	     > diff-$ubuntu-$trisquel$suffix.txt
    done
done

exit 0
