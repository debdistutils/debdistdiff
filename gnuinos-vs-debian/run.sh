#!/bin/sh

# Copyright (C) 2023 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e

DEBDISTDIFF=${DEBDISTDIFF:-debdistdiff}

DISTS_DEBIAN=${DISTS_DEBIAN:-/tmp/debian}
DISTS_GNUINOS=${DISTS_GNUINOS:-/tmp/gnuinos}

for suffix in "" "-updates" "-security"; do
    time $DEBDISTDIFF -v --arch amd64 --dist $DISTS_DEBIAN --dist $DISTS_GNUINOS \
	 chimaera$suffix bullseye$suffix:main \
	 > diff-debian-bullseye-gnuinos-chimaera$suffix.txt
done

time $DEBDISTDIFF -v --arch amd64 --dist $DISTS_DEBIAN --dist $DISTS_GNUINOS \
     daedalus bookworm:main \
     > diff-debian-bookworm-gnuinos-daedalus.txt

exit 0
