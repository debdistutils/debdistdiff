#!/bin/sh

# Copyright (C) 2023-2024 Simon Josefsson <simon@josefsson.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -e
set -x

DISTS_UBUNTU=${DISTS_UBUNTU:-/tmp/ubuntu}
DISTS_TRISQUEL=${DISTS_TRISQUEL:-/tmp/trisquel}
DISTS_DEBIAN=${DISTS_DEBIAN:-/tmp/debian}
DISTS_DEVUAN=${DISTS_DEVUAN:-/tmp/devuan}
DISTS_GNUINOS=${DISTS_GNUINOS:-/tmp/gnuinos}
DISTS_PUREOS=${DISTS_PUREOS:-/tmp/pureos}

if ! test -e $DISTS_UBUNTU; then
    time env GIT_LFS_SKIP_SMUDGE=1 git clone --depth 1 https://gitlab.com/debdistutils/dists/ubuntu.git $DISTS_UBUNTU
fi
cd $DISTS_UBUNTU
for f in dists/*/Release; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
for f in dists/*/*/binary-amd64/Packages; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
time git lfs checkout
cd $OLDPWD

if ! test -e $DISTS_TRISQUEL; then
    time env GIT_LFS_SKIP_SMUDGE=1 git clone --depth 1 https://gitlab.com/debdistutils/dists/trisquel.git $DISTS_TRISQUEL
fi
cd $DISTS_TRISQUEL
for f in dists/*/Release; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
for f in dists/*/*/binary-amd64/Packages; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
time git lfs checkout
cd $OLDPWD

if ! test -e $DISTS_DEBIAN; then
    time env GIT_LFS_SKIP_SMUDGE=1 git clone --depth 1 https://gitlab.com/debdistutils/dists/debian.git $DISTS_DEBIAN
fi
cd $DISTS_DEBIAN
for f in dists/*/Release; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
for f in dists/*/*/binary-amd64/Packages; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
time git lfs checkout
cd $OLDPWD

if ! test -e $DISTS_DEVUAN; then
    time env GIT_LFS_SKIP_SMUDGE=1 git clone --depth 1 https://gitlab.com/debdistutils/dists/devuan.git $DISTS_DEVUAN
fi
cd $DISTS_DEVUAN
for f in dists/*/Release; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
for f in dists/*/*/binary-amd64/Packages; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
time git lfs checkout
cd $OLDPWD

if ! test -e $DISTS_GNUINOS; then
    time env GIT_LFS_SKIP_SMUDGE=1 git clone --depth 1 https://gitlab.com/debdistutils/dists/gnuinos.git $DISTS_GNUINOS
fi
cd $DISTS_GNUINOS
for f in dists/*/Release; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
for f in dists/*/*/binary-amd64/Packages; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
time git lfs checkout
cd $OLDPWD

if ! test -e $DISTS_PUREOS; then
    time env GIT_LFS_SKIP_SMUDGE=1 git clone --depth 1 https://gitlab.com/debdistutils/dists/pureos.git $DISTS_PUREOS
fi
cd $DISTS_PUREOS
for f in dists/*/Release; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
for f in dists/*/*/binary-amd64/Packages; do
    git lfs fetch -I $(dirname $f)/$(readlink $f)
done
time git lfs checkout
cd $OLDPWD

exit 0
