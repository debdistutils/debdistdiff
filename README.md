# debdistdiff

## What is debdistdiff?

Debdistdiff is a tool to compare two Debian archives and summarize the
differences.

## Example Output

Sample outputs are published here:
https://debdistutils.gitlab.io/debdistdiff/

These are generated with the debdistdiff tool invoked through GitLab
CI/CD via [.gitlab-ci.yml](.gitlab-ci.yml) on recently fetched
Trisquel/Ubuntu archives, see
[trisquel-vs-ubuntu/run.sh](trisquel-vs-ubuntu/run.sh).

The output format is still changing, and we hope to add both text and
HTML outputs that are easy to read, as well as some stable format that
is easy to parse with tools.

## License

All files in the debdistdiff project are published under the AGPLv3+
see the file [COPYING](COPYING) or
[https://www.gnu.org/licenses/agpl-3.0.en.html].

## Installation and dependencies

Put the `debdistdiff` file in your PATH after installing the following
dependencies:

- Perl `apt-get install perl`
- Data::Dumper `apt-get install libdata-dmp-perl`
- IO::Uncompress `apt-get install libio-compress-lzma-perl`

## Usage

```
Usage: debdistdiff [--dists PATH]... [--arch ARCH] DIST-SRC DIST-DST
```

Compare two Debian-style archives and print a summary of differences.

The following are shown:

  1) A count of identical package that exists in DIST-DST.  Identical
     package means that it exists under the same name, version number
     and hash value.

  2) The names of binary packages that are in DIST-SRC and are not in
     DIST-DST.

  3) The names of binary packages that are in both DIST-SRC and
     DIST-DST with different version number or hash value.

The number of packages in each category is also reported.  The binary
packages are sorted under their corresponding source package name,
which is also shown.

If --arch is not specified, the first architecture in the Release file
for DIST-DST is used.

DIST-SRC and DIST-DST are Debian archive distribution names (e.g.,
aramo) and may optionally specify components (e.g., main), by
specifying DIST as SUITE[:COMPONENT,COMPONENT,...].  Binary packages
in all components of each suite are inspected by default, but you may
restrict the set to search by specifying the "COMPONENT,COMPONENT,..."
part of DIST.  For example, specifying jammy:main and trisquel:main
will only compare the 'main' components of each suite.

OPTIONS:
```
   --help          display this help and exit
   --version       output version information and exit

  -v, --verbose    explain what is being done
  -d, --debug      provide lots of debug output

  --modified-sources
                   instead of showing a summary, print the
                   source package names (without versions)
                   corresponding to all modified binary packages.
  --added-sources
                   instead of showing a summary, just list
                   the source package names corresponding to
                   all added binary packages.

   --dists=PATH    add PATH to where to look for archives
   --arch=ARCH     add ARCH to architecture to search
```

EXAMPLE

The following compares Ubuntu jammy with Trisquel aramo, looking for
their Debian archives under /mnt/ubuntu and /mnt/trisquel/packages,
and comparing only the amd64 binary architecture.

```
debdistdiff --arch amd64 --dists /mnt/ubuntu
            --dists /mnt/trisquel/packages
            jammy aramo
```

Considering that Trisquel aramo only imports Ubuntu jammy main and
universe components, and merges them into main, the command above will
produce a large output.  A more practical example may look like:

```
debdistdiff --arch amd64
            --dists /mnt/ubuntu
            --dists /mnt/trisquel/packages
            jammy:main,universe
            aramo
```

The following compares the 'main' and 'universe' sections of jammy
with aramo, using a merged pool that contains both Ubuntu and Trisquel
packages side-by-side.

```
debdistdiff --arch amd64 --dists /mnt/pool
             jammy:main,universe aramo
```
To compare two different archives for the same distribution (i.e.,
archives published at different times), understand that debdistdiff
uses the --dists PATHS in reverse when searching for DIST-DST:

```
debdistdiff --arch amd64
            --dists /mnt/trisquel-old
            --dists /mnt/trisquel-new
            aramo
            aramo
```

The Release file for Debian buster's security fixes specify components
as 'updates/main' but Packages file are not in the proper location, so
override it to use component 'main' instead to compare it with
PureOS's amber-security archive:

```
debdistdiff --verbose --arch amd64
             --dists /mnt/debian --dists /mnt/pureos
             buster/updates:main
             amber-security
```
